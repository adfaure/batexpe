This repository contains a set of Go tools around
[Batsim](https://gitlab.inria.fr/batsim/batsim) to simplify experiments.

## Install
```bash
go get gitlab.inria.fr/batsim/batexpe/cmd/robin
```

## Links to subprojects
- [robin](doc/robin.md) manages the execution of **one** simulation
